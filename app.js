const app = Vue.createApp({
  data() {
    return {
      keydownEventData: "OUTPUT",
      keyEnterEventData: "OUTPUT",
    };
  },
  methods: {
    alertText(textToAlert = "Hello") {
      alert(textToAlert);
    },
    handleKeyDownEvent(event) {
      this.keydownEventData = event.target.value;
    },
    handleKeyEnterEvent(event) {
      this.keyEnterEventData = event.target.value;
    },
  },
});
app.mount("#assignment");
